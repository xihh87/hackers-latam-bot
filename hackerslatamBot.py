#!/usr/bin/python3
# -*- coding: utf-8 -*-
# BOT DE HACKERS LATAM
# Autor: ZoRrO
#LICENCIA GPL-3 O SUPERIOR
import telebot # Librería de la API del bot.
import time # Librería para hacer que el programa que controla el bot no se acabe.
import random # para la funcion random
import urllib.request, json  #para parsear json  y obtener el precio deo bitcoin desde https://coindesk.com
from urllib.request import Request, urlopen
import pickle
import os
import sys
import logging
import configparser
config = configparser.ConfigParser()
config.read('config.ini')
API = config['DEFAULT']['api']

bot = telebot.TeleBot(API)

#Apartir de aqui son las funciones definidas por los comandos a los cuales responde el bot, como todos son iguales solo se explica uno

@bot.message_handler(commands=['start']) # Indicamos que lo siguiente va a controlar el comando '/star'
def saludo(m):
	''' Envia un saludo a la gente'''
	cid = m.chat.id			# Guardamos el ID de la conversación para poder responder.
	bot.send_message(cid, "Gracias :-)")	# Con la función 'send_message()' del bot, enviamos al ID almacenado el texto que queremos.

@bot.message_handler(commands=['hola'])
def hola(m):
	cid = m.chat.id
	vid = open('img/saludo.richd.mp4', 'rb')
	bot.send_message( cid, 'Ola k ase')
	bot.send_video(cid, vid)

@bot.message_handler(commands=['tira'])
def tira(m):
	cid = m.chat.id
	bot.send_message( cid, 'Pregunta de Tira')

@bot.message_handler(commands=['kosita'])
def kosita(m):
	cid = m.chat.id
	bot.send_message( cid, 'Cuánto amor!!')

@bot.message_handler(commands=['firma'])
def firma(m):
	cid = m.chat.id
	bot.send_message( cid, 'pásame tu credencial de elector de una vez para firmar por Marichuy!')

frases_milenial = [
    "esto no es touch???",
    "se tienen que usar botones???",
    "donde se conecta  el cargador???",
    "hay que caminar para llegar ???",
    "no sale en mi aifon????",
    "cual es la contraseña del guifi???",
    "Tas ChaV0 Ruk0",
    "Pasame tu feis",
    "pedian sillas en la escuela???",
    "lo vi en yiutuib",
    "que es /rtfm ???",
    "Pero sin chile",
    "buenos días solecito",
    "solo quiero ser popular",
    "¿donde esta mi bitcoin?",
    "¿qué es tonyan?",
    "eso es del siglo pasado!",
    "uy! en el metro no!",
    "sólo escucho música electrónica"
]

def escoger_frase(frases):
    n = len(frases)
    msg = frases[random.randrange(n)]
    return msg

@bot.message_handler(commands=['milenians'])
def milen(m):
    msg = escoger_frase(frases_milenial)
    bot.send_message(m.chat.id, msg)

@bot.message_handler(commands=['popular'])

def popular(m):
	cid = m.chat.id
	bot.send_message( cid, '¿popular? Primero dime quién eres.')

@bot.message_handler(commands=['nsa'])
def nsa(m):
	cid = m.chat.id
	bot.send_message( cid, 'Ese si es Juaquer')

@bot.message_handler(commands=['nsaN'])
def nsa2(m):
	cid = m.chat.id
	bot.send_message( cid, 'Y0 no soy Juaquer')

@bot.message_handler(commands=['noches'])
def nsa2(m):
        cid = m.chat.id
        bot.send_message( cid, 'Descansen 💋💋💋')

@bot.message_handler(commands=['canales'])
def canales(m):
        cid = m.chat.id
        bot.send_message( cid, 'https://t.me/HackLA\nhttps://t.me/libromiespalda\nhttps://t.me/autodefensadigital\nhttps://t.me/SembrarEnElBarrio\n')

@bot.message_handler(commands=['dias'])
def dias(m):
        cid = m.chat.id
        video = open('img/dias.mp4', 'rb')
        bot.send_video(cid, video)

@bot.message_handler(commands=['lurkeo'])
def lurkeo(m):
        cid = m.chat.id
        video = open('img/lurkeo.mp4', 'rb')
        bot.send_video(cid, video)

@bot.message_handler(commands=['nerd'])
def nerd(m):
	cid = m.chat.id
	sti = open('img/nerd.jpg', 'rb')
	bot.send_sticker(cid, sti)

@bot.message_handler(commands=['colimita'])
def colimita(m):
        cid = m.chat.id
        message = "El glorioso vulcán de Colima se ha quedado afuera de estado de Colima."
        bot.send_message(cid, message)

@bot.message_handler(commands=['cdmx'])
def cdmx(m):
        cid = m.chat.id
        bot.send_message( cid, 'Las provincias también existen!!')

frases_chavorruco = [
        "antes sí que era bonito",
        "los jóvenes van muy deprisa",
        "en mis tiempos era el grunge",
	"aquí siempre se ha hecho así",
	"vamos como locos!",
	"sabe el diablo más por viejo que por diablo",
	"estas nuevas generaciones no saben...",
	"que no conoces qué es un disquette??"
]

@bot.message_handler(commands=['chavorruco'])
def chavorruco(m):
        msg = elegir_frase(frases_chavorruco)
        bot.send_message(m.chat.id, msg)

@bot.message_handler(commands=['mezcal'])
def mezcal(m):
	cid = m.chat.id
	numero = random.randrange(3)
	gif = {
	0:open('img/mezcal1.mp4', 'rb'),
	1:open('img/mezcal2.mp4', 'rb'),
	2:open('img/mezcal3.mp4', 'rb'),
	}
	video = gif[numero]
	bot.send_video(cid, video)

@bot.message_handler(commands=['troll'])
def troll(m):
	cid = m.chat.id
	video = open ('img/troll.mp4', 'rb')
	bot.send_video(cid, video)

@bot.message_handler(commands=['trampa'])
def trampa(m):
        cid = m.chat.id
        vid = open('img/trampaa.gif', 'rb')
        bot.send_video(cid, vid)


@bot.message_handler(commands=['popcorn'])
def palomita(m):
        cid = m.chat.id
        vid = open('img/popc.mp4', 'rb')
        bot.send_video(cid, vid)

####################################################################################################################################################
####################################################################################################################################################
#El comando ayuda, siempre estara al final para que no se pierda entre los nuevos comando que se agregue
#Favor de agregar en esta sección si agregan algun comando nuevo al bot, para mantener actualziado la orden "ayura"
ayura = """/start - Saludo de Mazorca
/hola - Saludo
/nsa - es jueaker
/nsaN - no soy juaquer
/milenians - frases milenians al azar
/tira - pregunta tira
/rtfm - recomendacion de lectura
/canales - Listado de canales del grupo
/popular - ;)
/lurkeo - gif
/nerd - jpg
/dias - gif
/noches - gif
/colimita - curiosidad acerca del lugar
/mezcal - gif
/trampa - gif
/troll - gif
/ayura - Para mostrar este mensaje de nuevo
"""

@bot.message_handler(commands=['ayura'])
def ayuda(m):
        bot.send_message(m.chat.id, ayura)


if __name__ == "__main__":
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as e:
            logger.error(e)
            break
